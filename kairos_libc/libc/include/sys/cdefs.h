#ifndef _sys_cdefs_h
#define _sys_cdefs_h 1

#define __kairos_libc 1
#define __STDC_NO_THREADS__ 0
#define __BEGIN_DECLS 1
#define __END_DECLS 1

#endif
#ifndef _types__typedef_size_t_h
#define _types__typedef_size_t_h

typedef long unsigned int size_t;
typedef long unsigned int rsize_t;
typedef long ssize_t;

#endif
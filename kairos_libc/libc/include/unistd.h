#ifndef _unistd_h
#define _unistd_h 1

#include <sys/cdefs.h>
#include <sys/types.h>

int chdir(const char *path);

#endif